FROM php:7.1-apache

COPY conf/php.ini /usr/local/etc/php/

RUN a2enmod rewrite

ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

# Dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    mysql-client \
    libjpeg-dev \
    libmagickwand-dev \
    libmcrypt-dev \
    zip \
    git \
  && docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
  && docker-php-ext-install gd \
  && docker-php-ext-install zip \
  && docker-php-ext-install exif \
  && docker-php-ext-install mbstring \
  && docker-php-ext-install mcrypt \
  && docker-php-ext-install mysqli pdo pdo_mysql \
  && rm -rf /var/lib/apt/lists/*

RUN cd /var/www/html && git clone https://baloghszilveszter@bitbucket.org/baloghszilveszter/worksheet.git /var/www/html
RUN chmod 777 -R /var/www/html/cache
RUN chmod 777 -R /var/www/html/uploads

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN chmod +x /var/www/html/composer.phar
RUN /var/www/html/composer.phar install --no-dev
