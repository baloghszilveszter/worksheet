<?php
namespace Controllers;

use Models\UsedMaterialsModel;
use Models\UsersModel;
use Models\CompaniesModel;
use Models\WorksheetsModel;
use Nette\Mail\SmtpMailer;
use Nette\Mail\Message;
use Mpdf\Mpdf;

class WorksheetsController extends ControllerBase
{
    public function actionIndex($filter)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $year = date('Y');
        $month = 'all';
        if ($filter) {
            $filterParams = explode('-', $filter);
            $year = $filterParams[0];
            $month = $filterParams[1];
        }

        $this->title = 'Munkalapok listája';
        $this->render('index', [
            'users' => $this->getUsers(),
            'companies' => $this->getCompanies(),
            'worksheets' => WorksheetsModel::get()->getWorksheets($year, $month),
            'months' => $this->getMonths(),
            'selectedMonth' => $month,
            'years' => array_unique(WorksheetsModel::get()->getYears()+[$year => $year]),
            'selectedYear' => $year,
        ]);
    }

    public function actionNew()
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $errors = [];
        $form_values = [
            'serial_num' => time(),
            'user_id' => $_SESSION['user']['id'],
        ];
        if (isset($_POST['worksheet']) && !empty($_POST['worksheet'])) {
            $data = $_POST['worksheet'];
            if ($data['company_id'] == '') {
                $errors['company_id'] = 'Name can not be empty';
            }
            if (!isset($data['billable'])) {
                $errors['billable'] = 'Kötelező mező';
            }
            if ($data['work_duration'] == '') {
                $errors['work_duration'] = 'Kötelező mező';
            } else {
                if (!is_numeric($data['work_duration'])) {
                    $errors['work_duration'] = 'Nem szám';
                }
            }
            if ($data['delivery_date'] == '') {
                $errors['delivery_date'] = 'Kötelező mező';
            } else {
                if ($data['delivery_date'] > date('Y-m-d')) {
                    $errors['delivery_date'] = 'nem lehet nagyobb';
                }
            }
            if (!isset($data['needed_action'])) {
                $errors['needed_action'] = 'Kötelező mező';
            }

            if (empty($errors)) {
                if (WorksheetsModel::get()->createWorksheet($data)) {
                    $this->redirect('/worksheets/index');
                } else {
                    //TODO
                    die('fail');
                }
            }
            $form_values = [
                'serial_num' => $data['serial_num'],
                'company_id' => $data['company_id'],
                'problem_desc' => $data['problem_desc'],
                'delivery_date' => $data['delivery_date'],
                'work_duration' => $data['work_duration'],
                'work_desc' => $data['work_desc'],
                'user_id' => $data['user_id'],
            ];
            if (isset($data['billable'])) {
                $form_values['billable'] = $data['billable'];
            }
            if (isset($data['needed_action'])) {
                $form_values['needed_action'] = $data['needed_action'];
            }
        }

        $this->title = 'Új munkalap';
        $this->render('form', [
            'user_name' => $_SESSION['user']['name'],
            'companies' => $this->getCompanies(),
            'errors' => $errors,
            'form_values' => $form_values,
        ]);
    }

    public function actionEdit($id)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $worksheet = WorksheetsModel::get()->getWorksheetById($id);
        if (!$worksheet) {
            http_response_code(404);
            die('fail');
        }
        $usedMaterials = UsedMaterialsModel::get()->getMaterialsByWorksheetId($id);

        $errors = [];
        if (isset($_POST['worksheet']) && !empty($_POST['worksheet'])) {
            $data = $_POST['worksheet'];
            if ($data['company_id'] == '') {
                $errors['company_id'] = 'Kötelező mező';
            }
            if (!isset($data['billable'])) {
                $errors['billable'] = 'Kötelező mező';
            }
            if ($data['work_duration'] == '') {
                $errors['work_duration'] = 'Kötelező mező';
            } else {
                if (!is_numeric($data['work_duration'])) {
                    $errors['work_duration'] = 'Nem szám';
                }
            }
            if ($data['delivery_date'] == '') {
                $errors['delivery_date'] = 'Kötelező mező';
            } else {
                if ($data['delivery_date'] > date('Y-m-d')) {
                    $errors['delivery_date'] = 'Nem lehet újabb a mai napnál';
                }
            }
            if (!isset($data['needed_action'])) {
                $errors['needed_action'] = 'Kötelező mező';
            }

            if (empty($errors)) {
                if (WorksheetsModel::get()->updateWorksheet($id, $data)) {
                    $this->redirect('/worksheets/index');
                } else {
                    //TODO
                }
            }
            $form_values = [
                'serial_num' => $data['serial_num'],
                'company_id' => $data['company_id'],
                'problem_desc' => $data['problem_desc'],
                'delivery_date' => $data['delivery_date'],
                'work_duration' => $data['work_duration'],
                'billable' => $data['billable'],
                'work_desc' => $data['work_desc'],
                'needed_action' => $data['needed_action'],
                'user_id' => $data['user_id'],
                'material_name' => isset($data['material_name']) ? $data['material_name'] : [],
                'material_price' => isset($data['material_price']) ? $data['material_price'] : [],
            ];
        } else {
            $form_values = [
                'serial_num' => $worksheet['serial_num'],
                'company_id' => $worksheet['company_id'],
                'problem_desc' => $worksheet['problem_desc'],
                'delivery_date' => $worksheet['delivery_date'],
                'work_duration' => $worksheet['work_duration'],
                'billable' => $worksheet['billable'],
                'work_desc' => $worksheet['work_desc'],
                'needed_action' => $worksheet['needed_action'],
                'user_id' => $worksheet['user_id'],
                'materials' => $usedMaterials,
            ];
        }

        $this->title = 'Munkalap módosítása';
        $this->render('form', [
            'user_name' => $_SESSION['user']['name'],
            'companies' => $this->getCompanies(),
            'errors' => $errors,
            'form_values' => $form_values,
        ]);
    }

    public function actionDelete($id)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $worksheet = WorksheetsModel::get()->getWorksheetById($id);
        if (!$worksheet) {
            http_response_code(404);
            die('fail');
        }

        if (WorksheetsModel::get()->deleteWorksheet($id)) {
            $this->redirect('/worksheets/index');
        } else {
            //TODO
        }
    }

    public function actionShow($id)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $worksheet = WorksheetsModel::get()->getWorksheetById($id);
        if (!$worksheet) {
            http_response_code(404);
            die('fail');
        }

        $this->disableLayout = true;
        header('Cache-Control: max-age=0');
        header('Content-Type: application/pdf');
        header('Pragma: public');

        if ($worksheet['uploaded'] == 1) {
            $fileName = BASE_PATH . '/uploads/'  . $worksheet['serial_num'] . '.pdf';
            header('Content-Disposition: inline; filename="'.basename($fileName).'"');
            header('Content-Length: ' . filesize($fileName));
            readfile($fileName);
            exit;
        }

        $html = $this->render('pdf', [
            'worksheet' => $worksheet,
            'used_materials' => UsedMaterialsModel::get()->getMaterialsByWorksheetId($id),
            'companies' => $this->getCompanies(),
            'user' => $_SESSION['user'],
        ]);

        $stylesheet = file_get_contents(BASE_PATH . '/public/css/worksheet.css');
        $pdf = new Mpdf(['tempDir' => BASE_PATH . '/cache/mpdf']);
        $pdf->WriteHTML($stylesheet, 1);
        $pdf->shrink_tables_to_fit = 0;
        $pdf->WriteHTML($html, 2);
        $pdf->Output($worksheet['serial_num'] . '.pdf', 'I');
    }

    public function actionSend()
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }


        if (isset($_POST['worksheet']) && !empty($_POST['worksheet'])) {
            $data = $_POST['worksheet'];
            $worksheet = WorksheetsModel::get()->getWorksheetById($data['id']);
            if (!$worksheet || $data['email'] == '') {
                http_response_code(404);
                die('fail');
            }

            $this->disableLayout = true;
            $html = $this->render('pdf', [
                'worksheet' => $worksheet,
                'companies' => $this->getCompanies(),
                'user' => $_SESSION['user'],
            ]);

            $stylesheet = file_get_contents(BASE_PATH . '/public/css/worksheet.css');
            $pdf = new Mpdf(['tempDir' => BASE_PATH . '/cache/mpdf']);
            $pdf->WriteHTML($stylesheet, 1);
            $pdf->shrink_tables_to_fit = 0;
            $pdf->WriteHTML($html, 2);
            $content = $pdf->Output('', 'S');
            if ($content) {
                $mailer = new SmtpMailer([
                    'host' => 'smtp_relay',
                ]);
                $mail = new Message;
                $mail->setFrom('Worksheet <worksheet@fotex.net>')
                    ->addTo($data['email'])
                    ->setSubject('Munkalap érkezett')
                    ->setBody("Tisztelt Címzett!\n\nÖnnek aláírásra váró munkalapja érkezett. Kérem, hogy aláírásával igazolja az elvégzett munkát és juttassa vissza a munkalapot elektronikus vagy papír formában.\n\nÜdvözlettel,\nFotexnet IT")
                    ->addAttachment($worksheet['serial_num'] . '.pdf', $content, 'application/pdf');
                $mailer->send($mail);
            }
            $this->redirect('/worksheets/index');
        }
    }


    public function actionUpload()
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        if (isset($_POST['upload']) && !empty($_POST['upload'])) {
            $data = $_POST['upload'];
            $worksheet = WorksheetsModel::get()->getWorksheetById($data['id']);
            if (!$worksheet) {
                http_response_code(404);
                die('fail');
            }

            if ($_FILES['pdf_file']['name'] != "") {
                $path = pathinfo($_FILES['pdf_file']['name']);
                $ext = $path['extension'];
                if ($ext != 'pdf') {
                    die('fail');
                }
                move_uploaded_file($_FILES['pdf_file']['tmp_name'], BASE_PATH . '/uploads/'  . $worksheet['serial_num'] . "." . $ext);
            } else {
                die('fail');
            }

            if (WorksheetsModel::get()->setUploaded($worksheet['id'])) {
                $this->redirect('/worksheets/index');
            } else {
                //TODO
            }
        }
    }

    private function getUsers()
    {
        $users = [];
        foreach (UsersModel::get()->getUsers() as $user) {
            $users[$user['id']] = $user['name'];
        }
        return $users;
    }

    private function getCompanies()
    {
        $companies = [];
        foreach (CompaniesModel::get()->getCompanies() as $company) {
            $companies[$company['id']] = $company['name'];
        }
        return $companies;
    }

    private function getMonths()
    {
        return [
            'all' => 'összes',
            1 => "január",
            2 => "február",
            3 => "március",
            4 => "április",
            5 => "május",
            6 => "június",
            7 => "július",
            8 => "augusztus",
            9 => "szeptember",
            10 => "október",
            11 => "november",
            12 => "december"
        ];
    }
}
