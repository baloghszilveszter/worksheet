<?php
namespace Controllers;

use Models\CompaniesModel;

class CompaniesController extends ControllerBase
{
    public function actionIndex()
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $this->title = 'Cégek listája';
        $this->render('index', [
            'companies' => CompaniesModel::get()->getCompanies(),
        ]);
    }

    public function actionNew()
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $errors = [];
        $form_values = [];
        if (isset($_POST['company']) && !empty($_POST['company'])) {
            $data = $_POST['company'];
            if ($data['name'] == '') {
                $errors['name'] = 'A mező kitöltése kötelező.';
            }
            if ($data['hourly_wage'] == '') {
                $errors['hourly_wage'] = 'A mező kitöltése kötelező.';
            } else {
                if (!is_numeric($data['hourly_wage'])) {
                    $errors['hourly_wage'] = 'Nem szám.';
                }
            }
            if (empty($errors)) {
                if (CompaniesModel::get()->createCompany($data)) {
                    $this->redirect('/companies/index');
                } else {
                    //TODO
                }
            }
            $form_values = [
                'name' => $data['name'],
                'tax_number' => $data['tax_number'],
                'address' => $data['address'],
                'hourly_wage' => $data['hourly_wage'],
            ];
        }

        $this->title = 'Új cég';
        $this->render('form', [
            'errors' => $errors,
            'form_values' => $form_values,
        ]);
    }

    public function actionEdit($id)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $company = CompaniesModel::get()->getCompanyById($id);
        if (!$company) {
            http_response_code(404);
            die('fail');
        }

        $errors = [];
        if (isset($_POST['company']) && !empty($_POST['company'])) {
            $data = $_POST['company'];
            if ($data['name'] == '') {
                $errors['name'] = 'Name can not be empty';
            }
            if (empty($errors)) {
                if (CompaniesModel::get()->updateCompany($id, $data)) {
                    $this->redirect('/companies/index');
                } else {
                    //TODO
                }
            }
            $form_values = [
                'name' => $data['name'],
                'tax_number' => $data['tax_number'],
                'address' => $data['address'],
            ];
        } else {
            $form_values = [
                'name' => $company['name'],
                'tax_number' => $company['tax_number'],
                'address' => $company['address'],
                'hourly_wage' => $company['hourly_wage'],
            ];
        }

        $this->title = 'Cég módosítása';
        $this->render('form', [
            'errors' => $errors,
            'form_values' => $form_values,
        ]);
    }

    public function actionDelete($id)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $company = CompaniesModel::get()->getCompanyById($id);
        if (!$company) {
            http_response_code(404);
            die('fail');
        }

        if (CompaniesModel::get()->deleteCompany($id)) {
            $this->redirect('/companies/index');
        } else {
            //TODO
        }
    }
}
