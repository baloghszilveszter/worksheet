<?php
namespace Controllers;

use Models\UsersModel;
use Models\WorksheetsModel;

class SiteController extends ControllerBase
{
    public function actionIndex($arg)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        $this->title = 'Statisztika';
        
        $this->render('index', [
            'incomes' => WorksheetsModel::get()->getIncomes(),
            'incomes_count_by_status' => [
                'closed' => [
                    'value' => WorksheetsModel::get()->getIncomesUploadedStatusCount(1)['count'],
                    'name' => 'Lezárt'
                ],
                'opened' => [
                    'value' => WorksheetsModel::get()->getIncomesUploadedStatusCount(0)['count'],
                    'name' => 'Nyitott'
                    ]
            ],
        ]);
    }

    public function actionLogin()
    {
        if (isset($_SESSION['user'])) {
            $this->redirect('/site/index');
        }

        $form_values = [];
        $errors = [];
        if (isset($_POST['login']) && !empty($_POST['login'])) {
            $data = $_POST['login'];

	        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
		        $errors['email'] = 'Az e-mail cím nem megfelelő.';
	        }

            if ($data['password'] == '') {
                $errors['password'] = 'A jelszó nem lehet üres.';
            }

            if (empty($errors)) {
                $user = UsersModel::get()->getUserByEmail($data['email']);
                if (!empty($user) && $user['password'] == hash('md5',$data['password'])) {
                    $_SESSION['user'] = $user;
                    $this->redirect('/site/index');
                } else {
                    $errors['password'] = 'Hibás felhasználónév vagy jelszó';
                }
            }
            $form_values = [
                'email' => $data['email'],
                'password' => $data['password'],
            ];
        }

        $this->title = 'bejelentkezés';
        $this->setLayout('login');
        $this->render('login', [
            'errors' => $errors,
            'form_values' => $form_values,
        ]);
    }

    public function actionLogout()
    {
        session_destroy();
        $this->redirect('/site/index');
    }
}
