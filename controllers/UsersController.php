<?php
namespace Controllers;

use Models\UsersModel;

class UsersController extends ControllerBase
{
    public function actionEdit($id)
    {
        if (!isset($_SESSION['user'])) {
            $this->redirect('/site/login');
        }

        if (empty($id) || $_SESSION['user']['id'] != $id) {
            http_response_code(404);
            die('fail');
        }

        $errors = [];
        if (isset($_POST['user']) && !empty($_POST['user'])) {
            $data = $_POST['user'];
            if ($data['name'] == '') {
                $errors['name'] = 'Name can not be empty';
            }
            if ($data['new_password'] != '' && $data['repeat_new_password'] == '') {
                $errors['password'] = 'new pw can not be empty';
            }
            if ($data['new_password'] != '' && $data['repeat_new_password'] != '') {
                if ($data['new_password'] != $data['repeat_new_password']) {
                    $errors['password'] = 'A két jelszó nem egyezik meg';
                } else {
                    $data['password'] = md5($data['new_password']);
                }
            }
            if (empty($errors)) {
                unset($data['new_password']);
                unset($data['repeat_new_password']);
                if (UsersModel::get()->updateUser($id, $data)) {
                    $this->redirect('/site/index');
                } else {
                    //TODO
                }
            }
            $form_values = [
                'name' => $data['name'],
                'signature' => $data['signature'],
                'new_password' => $data['new_password'],
                'repeat_new_password' => $data['repeat_new_password'],
            ];
        } else {
            $user = UsersModel::get()->getUserById($id);
            $form_values = [
                'name' => $user['name'],
                'signature' => $user['signature'],
            ];
        }

        $this->title = 'Beállítások';
        $this->render('edit', [
            'errors' => $errors,
            'form_values' => $form_values,
        ]);
    }
}
