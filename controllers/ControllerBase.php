<?php
namespace Controllers;

abstract class ControllerBase
{
    public $controllerName;

    public $content;

    public $title = '';

    protected $disableLayout = false;

    protected $layout = 'main';

    public function __construct()
    {
        $reflect = new \ReflectionClass($this);
        $this->controllerName = lcfirst(str_replace('Controller', '', $reflect->getShortName()));
    }

    protected function render($view, $params = [])
    {
        $viewFile = '../views/' . $this->controllerName . '/' . $view . '.php';
        if (file_exists($viewFile)){
            $this->addProperty($params);
            $this->content = $this->getViewData($viewFile);
            if (!$this->disableLayout) {
                include('../views/layouts/' . $this->layout . '.php');
            }
            return $this->content;
        } else {
            die($viewFile.' file not exists');
        }
    }

    protected function redirect($url)
    {
        header("Location: $url");
    }

    protected function setLayout($layoutName)
    {
        $this->layout = $layoutName;
    }

    private function getViewData($viewFile)
    {
        ob_start();
        include $viewFile;
        $template = ob_get_contents();
        ob_end_clean();
        return $template;
    }

    private function addProperty($params = [])
    {
        foreach ($params as $propertyKey => $propertyVal) {
            $this->$propertyKey = $propertyVal;
        }
    }
}
