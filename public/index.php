<?php
header('Content-type: text/html; charset=UTF-8');
date_default_timezone_set('Europe/Budapest');
ini_set('display_errors', 1);
error_reporting(E_ALL);

require '../vendor/autoload.php';
require '../config.php';

use Library\Core;

$core = new Core();
$core->setDefaultController('site');
$core->setDefaultAction('index');
$core->process();
