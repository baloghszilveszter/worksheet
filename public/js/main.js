$(document).ready(function() {
    var $sigdiv = $("#jSignature");
    $sigdiv.jSignature({
        color: "#00f",
    });
    if ($('#user_signature').length) {
        if ($('#user_signature').val() != '') {
            $sigdiv.jSignature("setData", $('#user_signature').val());
        }
    }
    $sigdiv.bind('change', function(e){
        var datapair = $sigdiv.jSignature("getData", "default");
        $('#user_signature').attr('value', datapair);
    });
    $('#signature-reset').on('click', function(e) {
        e.preventDefault();
        $sigdiv.jSignature("reset");
        $('#user_signature').val('');
    });

    $('.datepicker').datepicker({
        'autoclose': true,
        'format': 'yyyy-mm-dd',
        'setEndDate': new Date(),
    });
});
$('#worksheetModal').on('show.bs.modal', function (e) {
    var $button = $(event.target);
    $('#worksheet_id').val($button.attr('data-id'));
});
$('#uploadModal').on('show.bs.modal', function (e) {
    var $link = $(event.target);
    $('#upload_id').val($link.attr('data-id'));
});