<form method="post">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="user_name">Név</label>
                <input type="text" name="user[name]" id="user_name" class="form-control" placeholder="név" required value="<?= isset($this->form_values['name']) ? $this->form_values['name'] : '' ?>">
                <?php if (isset($this->errors['name'])): ?>
                    <div><?= $this->errors['name'] ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="user_new_password">Új jelszó</label>
                <input type="password" name="user[new_password]" id="user_new_password" class="form-control" value="<?= isset($this->form_values['new_password']) ? $this->form_values['new_password'] : '' ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="user_repeat_new_password">Új jelszó ismét</label>
                <input type="password" name="user[repeat_new_password]" id="user_repeat_new_password" class="form-control" value="<?= isset($this->form_values['repeat_new_password']) ? $this->form_values['repeat_new_password'] : '' ?>">
                <?php if (isset($this->errors['password'])): ?>
                    <div><?= $this->errors['password'] ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="user_signature" name="user[signature]" value="<?= isset($this->form_values['signature']) ? $this->form_values['signature'] : '' ?>">
        <div class="col-md-4">
            <div class="form-group">
                <div class="clearfix">
                    <label class="control-label">Aláírás</label>
                    <span id="signature-reset" class="badge badge-danger badge-pill pull-right ml-auto">Törlés</span>
                </div>
                <div id="jSignature" class="form-control" style="padding: 0; height: auto;"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <button class="btn btn-md btn-primary btn-block" type="submit">Mentés</button>
            </div>
        </div>
    </div>
</form>