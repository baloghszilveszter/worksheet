<form method="post">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="company_name">Név</label>
                <input type="text" name="company[name]" id="company_name" class="form-control" value="<?= isset($this->form_values['name']) ? $this->form_values['name'] : '' ?>">
                <?php if (isset($this->errors['name'])): ?>
                    <div class="error"><?= $this->errors['name'] ?></div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="company_hourly_wage">Óradíj</label>
                <input type="text" id="company_hourly_wage" name="company[hourly_wage]" class="form-control" value="<?= isset($this->form_values['hourly_wage']) ? $this->form_values['hourly_wage'] : '' ?>">
                <?php if (isset($this->errors['hourly_wage'])): ?>
                    <div class="error"><?= $this->errors['hourly_wage'] ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="company_tax_number">Adószám</label>
                <input type="text" id="company_tax_number" name="company[tax_number]" class="form-control" value="<?= isset($this->form_values['tax_number']) ? $this->form_values['tax_number'] : '' ?>">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="company_address">Cím</label>
                <input type="text" id="company_address" name="company[address]" class="form-control" value="<?= isset($this->form_values['address']) ? $this->form_values['address'] : '' ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <button class="btn btn-md btn-primary btn-block" type="submit">Mentés</button>
            </div>
        </div>
    </div>
</form>
