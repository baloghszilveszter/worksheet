<div class="row">
    <div class="col-md-2 pull-right">
        <a class="btn btn-md btn-primary btn-block" href="/companies/new">Új cég</a>
    </div>
</div>

<?php if (!empty($this->companies)): ?>
<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Név</th>
            <th>Adószám</th>
            <th>Cím</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($this->companies as $i => $company): ?>
        <tr>
            <td><?= ($i+1) ?></td>
            <td><?= $company['name'] ?></td>
            <td><?= $company['tax_number'] ?></td>
            <td><?= $company['address'] ?></td>
            <td>
                <div class="pull-right">
                <div class="btn-group btn-group-xs">
                    <a class="btn btn-default" title="Szerkesztés" href="/companies/edit/<?= $company['id'] ?>"><i class="glyphicon glyphicon-edit"></i></a>
                    <a class="btn btn-danger" title="Törlés" href="/companies/delete/<?= $company['id'] ?>"><i class="glyphicon glyphicon-trash"></i></a>
                </div>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php else: ?>
    <h4>Nincs megjeleníthető adat.</h4>
<?php endif; ?>