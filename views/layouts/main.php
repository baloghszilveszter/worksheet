<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Munkalap nyílvántartó<?php if($this->title): ?> - <?= $this->title ?><?php endif; ?></title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/datepicker.css" rel="stylesheet">
    <link href="/css/dashboard.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <style>
        @media only screen and (max-width: 767px) {
            .sidebar {display:block !important; clear: both; }
        }
    </style>
</head>
<body>
    <script>
        var companies = [];
        var companyIncomes = [];
        var graphColors = [];
        var incomes = {};
        
        var StatusList = [];
        var StatusData = [];
        var graphColorsStatus = [];
        var incomes_by_status = {};
    </script>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Munkalap nyilvántartó</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <?php if (isset($_SESSION['user'])): ?>
                        <li <?php if ($this->controllerName == 'users'): ?>class="active"<?php endif; ?>><a href="/users/edit/<?= $_SESSION['user']['id'] ?>">Beállítások</a></li>
                        <li><a href="/site/logout">Kijelentkezés</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div>
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li <?php if ($this->controllerName == 'site'): ?>class="active"<?php endif; ?>><a href="/site/index">Lekérdezések</a></li>
                        <li <?php if ($this->controllerName == 'worksheets'): ?>class="active"<?php endif; ?>><a href="/worksheets/index">Munkalapok</a></li>
                        <li <?php if ($this->controllerName == 'companies'): ?>class="active"<?php endif; ?>><a href="/companies/index">Cégek</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <h1 class="page-header"><?= $this->title ?></h1>
                    <?= $this->content ?>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-datepicker.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/flashcanvas.js"></script>
    <![endif]-->
    <script src="/js/jSignature.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>
        if (!$.isEmptyObject(incomes)) {
            $.each(incomes, function (key, value) {
                companies.push(key);
                companyIncomes.push(value);

                var randomR = Math.floor((Math.random() * 130) + 100);
                var randomG = Math.floor((Math.random() * 130) + 100);
                var randomB = Math.floor((Math.random() * 130) + 100);
                var graphBackground = "rgb(" + randomR + ", " + randomG + ", " + randomB + ")";
                graphColors.push(graphBackground);
            });
            var ctx = document.getElementById('incomesChart');
            var myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data = {
                    labels: companies,
                    datasets: [{
                        backgroundColor: graphColors,
                        data: companyIncomes
                    }]
                }
            });
        }
    </script>

    <script>
        if (!$.isEmptyObject(incomes_by_status)) {

            $.each(incomes_by_status, function (key, value) {
                StatusList.push(value.name);
                StatusData.push(value.value);

                var randomR = Math.floor((Math.random() * 130) + 100);
                var randomG = Math.floor((Math.random() * 130) + 100);
                var randomB = Math.floor((Math.random() * 130) + 100);
                var graphBackground = "rgb(" + randomR + ", " + randomG + ", " + randomB + ")";
                graphColorsStatus.push(graphBackground);

            });

            var ctx = document.getElementById('incomesChartStat');
            var myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data = {
                    labels: StatusList,
                    datasets: [{
                        backgroundColor: graphColorsStatus,
                        data: StatusData
                    }]
                }
            });
        }
    </script>

    <script>
        $(document).ready(function(){
            var maxField = 9; //Input fields increment limitation
            var addButton = $('.add_button'); //Add button selector
            var wrapper = $('.field_wrapper'); //Input field wrapper
            var fieldHTML = `<div class="row material_row">
                <div class="col-xs-12"><h5><span class="label label-primary">Anyag #<span class="material_number"></span></span> <span class="label label-danger"><a href="javascript:void(0);" class="remove_button" style="color: white;"><span class="glyphicon glyphicon-remove"></span></a></span></h5></div>
                <div class="col-xs-12 col-md-4"><div class="form-group"><input type="text" name="worksheet[material_name][]" placeholder="neve" class="form-control" value=""/></div></div>
                <div class="col-xs-12 col-md-2"><div class="form-group"><input type="text" name="worksheet[material_price][]" placeholder="ára" class="form-control" value=""/></div></div>
                </div>`;
            var x = wrapper.children('.material_row').length; //Initial field counter is 1

            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if(x <= maxField){
                    x++; //Increment field counter
                    $(wrapper).append(fieldHTML); //Add field html
                    $(wrapper).children('.material_row').last().find('span.material_number').html((parseInt(x) ));
                    if(x > maxField)
                        {
                            addButton.addClass('disabled');
                        }
                }
            });

            //Once remove button is clicked
            $(wrapper).on('click', '.remove_button', function(e){
                e.preventDefault();
                x--; //Decrement field counter
                $(this).parents('div.material_row').remove(); //Remove field html
                recount_materials( $(wrapper).children('.material_row').find('h5') );
                addButton.removeClass('disabled');
            });

            var recount_materials = function(material_headlines){
                $.each(material_headlines, function( h5_number, headline ){  
                    $(headline).find('.material_number').html(h5_number + 1);
                });
            }
        });
    </script>
</body>
</html>

