<div class="container">
    <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Bejelentkezés</h2>
        <label for="login_email" class="sr-only">E-mail</label>
        <input type="email" name="login[email]" id="login_email" class="form-control" placeholder="E-mail" required autofocus value="<?= isset($this->form_values['email']) ? $this->form_values['email'] : '' ?>">
        <?php if (isset($this->errors['email'])): ?>
            <div><?= $this->errors['email'] ?></div>
        <?php endif; ?>
        <label for="login_password" class="sr-only">Jelszó</label>
        <input type="password" name="login[password]" id="login_password" class="form-control" placeholder="Jelszó" required value="<?= isset($this->form_values['password']) ? $this->form_values['password'] : '' ?>">
        <?php if (isset($this->errors['password'])): ?>
            <div><?= $this->errors['password'] ?></div>
        <?php endif; ?>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Belépés</button>
    </form>
</div>
