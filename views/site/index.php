<?php if (!empty($this->incomes)): ?>
<div class="row placeholders">

    <div class="col-xs-6 col-sm-3">
        <canvas style="width: 200px; height: 200px;" id="incomesChart"></canvas>
        <span class="text-muted">Cégenkénti árbevétel</span>
    </div>

    <div class="col-xs-6 col-sm-3">
        <canvas style="width: 200px; height: 200px;" id="incomesChartStat"></canvas>
        <span class="text-muted">Munkalap státuszok</span>
    </div>

</div>
<?php else: ?>
    <h4>Nincs megjeleníthető adat</h4>
<?php endif; ?>
<script>
    var incomes = <?= json_encode($this->incomes) ?>;
</script>
<script>
    var incomes_by_status = <?= json_encode($this->incomes_count_by_status) ?>;
</script>
