<table class="data-table">
    <tr>
        <td colspan="2" style="width: 60%;"><h4>Fotexnet Kereskedelmi és Szolgáltató Kft.</h4></td>
        <td style="width: 40%; text-align: right;">Tel.: (+36-1) 487 3600</td>
    </tr>
    <tr>
        <td colspan="2" style="width: 60%;">1126 Budapest Nagy Jenő u. 12.</td>
        <td style="width: 40%; text-align: right;">sysadmin@fotex.net</td>
    </tr>
    <tr>
        <td colspan="3" height="40"></td>
    </tr>
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 40%; text-align: center;"><h1>MUNKALAP</h1></td>
        <td style="width: 30%; text-align: right; vertical-align: bottom;">Sorszám: <strong><?= $this->worksheet['serial_num'] ?></strong></td>
    </tr>
    <tr>
        <td colspan="3" class="placer"></td>
    </tr>
    <tr>
        <td colspan="3" class="label">Megrendelő</td>
    </tr>
    <tr>
        <td colspan="3"><?= $this->companies[$this->worksheet['company_id']] ?></td>
    </tr>
    <tr>
        <td colspan="3" class="placer"></td>
    </tr>
    <tr>
        <td colspan="3" class="label">A hiba leírása</td>
    </tr>
    <tr>
        <td colspan="3" style="height: 150px; vertical-align: top;"><?= $this->worksheet['problem_desc'] ? $this->worksheet['problem_desc'] : '-' ?></td>
    </tr>
    <tr>
        <td colspan="3" class="placer"></td>
    </tr>
    <tr>
        <td style="width: 30%; text-align: center;" class="label">Kiszállás időpontja</td>
        <td style="width: 40%; text-align: center;" class="label">Munka időtartama</td>
        <td style="width: 30%; text-align: center;" class="label">Munkalap számlázandó?</td>
    </tr>
    <tr>
        <td style="text-align: center;"><?= $this->worksheet['delivery_date'] ?></td>
        <td style="text-align: center;"><?= $this->worksheet['work_duration'] ?></td>
        <td style="text-align: center;"><?= $this->worksheet['billable'] ? 'Igen' : 'Nem' ?></td>
    </tr>
    <tr>
        <td colspan="3" class="placer"></td>
    </tr>
    <tr>
        <td colspan="3" class="label">Felhasznált anyagok</td>
    </tr>
    <tr>
        <td colspan="3" style="height: 150px; vertical-align: top;">
            <?php if (!empty($this->used_materials)): ?>
                <table class="sub-data-table">
                <?php foreach ($this->used_materials as $material): ?>
                    <tr>
                        <td><?= $material['name'] ?></td>
                        <td><?= $material['price'] ?> Ft</td>
                    </tr>
                <?php endforeach; ?>
                </table>
            <?php else: ?>
                -
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="placer"></td>
    </tr>
    <tr>
        <td colspan="3" class="label">Elvégzett munka leírása</td>
    </tr>
    <tr>
        <td colspan="3" style="height: 150px; vertical-align: top;"><?= $this->worksheet['work_desc'] ? $this->worksheet['work_desc'] : '-' ?></td>
    </tr>

    <tr>
        <td colspan="3" class="placer"></td>
    </tr>
    <tr>
        <td style="width: 30%; text-align: center;" class="label">További intézkedés szükséges?</td>
        <td style="width: 40%; text-align: center;" class="label">A munkát végezte</td>
        <td style="width: 30%; text-align: center;" class="label">Elvégzését igazolja</td>
    </tr>
    <tr>
        <td style="width: 30%; text-align: center;"><?= $this->worksheet['needed_action'] ? 'Igen' : 'Nem' ?></td>
        <td style="width: 40%; text-align: center;"><?= $this->user['name'] ?></td>
        <td style="width: 30%;">Név:</td>
    </tr>
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 40%; text-align: center;"><img style="width: 30%;" src="<?= $this->user['signature'] ?>"></td>
        <td style="width: 30%;">Aláírás:</td>
    </tr>
</table>
