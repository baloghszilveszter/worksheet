<div class="row">
    <div class="col-md-2 pull-right">
        <a class="btn btn-md btn-primary btn-block" href="/worksheets/new">Új munkalap</a>
    </div>
</div>

<div class="btn-group">
    <div class="btn-group">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
            <?= $this->years[$this->selectedYear] ?>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <?php foreach ($this->years as $key => $name): ?>
                <li <?php if ($this->selectedYear == $key): ?>class="active"<?php endif; ?>><a href="/worksheets/index/<?= $key.'-'.$this->selectedMonth ?>"><?= $name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="btn-group">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
            <?= $this->months[$this->selectedMonth] ?>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <?php foreach ($this->months as $key => $name): ?>
                <li <?php if ($this->selectedMonth == $key): ?>class="active"<?php endif; ?>><a href="/worksheets/index/<?= $this->selectedYear.'-'.$key ?>"><?= $name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php if (!empty($this->worksheets)): ?>
<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Sorszám</th>
            <th>Megrendelő</th>
            <th>Munkát végezte</th>
            <th>Kiszállás dátuma</th>
            <th>Kiállítva</th>
            <th>Díj</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php $sumPrice = 0 ?>
        <?php foreach ($this->worksheets as $i => $worksheet): ?>
        <tr <?php if ($worksheet['uploaded'] == 1) {echo 'class="success"';} else {echo 'class="danger"';}  ?>>
            <?php
                $price = $worksheet['billable'] ? ($worksheet['work_duration'] * $worksheet['hourly_wage'] + $worksheet['materials_price']) : 0;
                $sumPrice += $price;
            ?>
            <td><?= ($i+1) ?></td>
            <td><a target="_blank" href="/worksheets/show/<?= $worksheet['id'] ?>"><?= $worksheet['serial_num'] ?></a></td>
            <td><?= $this->companies[$worksheet['company_id']] ?></td>
            <td><?= $this->users[$worksheet['user_id']] ?></td>
            <td><?= date('Y.m.d.', strtotime($worksheet['delivery_date'])) ?></td>
            <td><?= date('Y.m.d. H:i', strtotime($worksheet['created_at'])) ?></td>
            <td><?= $price ?> Ft</td>
            <td>
                <div class="pull-right">
                    <?php if ($worksheet['uploaded'] == 0): ?>
                    <div class="btn-group btn-group-xs">
                    <a class="btn btn-default" title="Szerkesztés" href="/worksheets/edit/<?= $worksheet['id'] ?>"><i class="glyphicon glyphicon-edit"></i></a>
                    <a class="btn btn-danger" title="Törlés" href="/worksheets/delete/<?= $worksheet['id'] ?>"><i class="glyphicon glyphicon-trash"></i></a>
                    <a class="btn btn-success" title="Feltöltés" href="" data-toggle="modal" data-target="#uploadModal">
                        <i data-id="<?= $worksheet['id'] ?>" class="glyphicon glyphicon-lock"></i>
                    </a>
                    <a class="btn btn-info" title="E-mail küldés" href="" data-toggle="modal" data-target="#worksheetModal">
                        <i data-id="<?= $worksheet['id'] ?>" class="glyphicon glyphicon-send"></i>
                    </a>
                    </div>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6"><b>Összesen:</b></td>
                <td><b><?= $sumPrice ?> Ft</b></td>
                <td></td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="modal fade" id="worksheetModal" tabindex="-1" role="dialog" aria-labelledby="worksheetModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="worksheet_form" method="post" action="/worksheets/send">
            <div class="modal-header">
                <h5 class="modal-title" id="worksheetModalLabel">Munkalap küldése e-mail-ben</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="worksheet_email">E-mail cím</label>
                            <input type="text" id="worksheet_email" name="worksheet[email]" class="form-control" value="">
                            <input type="hidden" id="worksheet_id" name="worksheet[id]" class="form-control" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
                <button type="submit" class="btn btn-primary">Küldés</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="upload_form" enctype="multipart/form-data" method="post" action="/worksheets/upload">
            <div class="modal-header">
                <h5 class="modal-title" id="uploadModalLabel">Munkalap feltöltése</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="upload_file">PDF fájl feltöltése</label>
                            <input type="file" id="upload_file" name="pdf_file" class="form-control" value="">
                            <input type="hidden" id="upload_id" name="upload[id]" class="form-control" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
                <button type="submit" class="btn btn-primary">Feltöltés</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php else: ?>
    <h4>Nincs megjeleníthető adat.</h4>
<?php endif; ?>