<form method="post">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="worksheet_company_id">Megrendelő</label>
                <input type="hidden" name="worksheet[serial_num]" id="worksheet_serial_num" class="form-control" value="<?= $this->form_values['serial_num'] ?>">
                <select name="worksheet[company_id]" id="worksheet_company_id" class="form-control">
                    <option value="">Kérem válasszon</option>
                    <?php foreach($this->companies as $companyId => $companyName): ?>
                        <option value="<?= $companyId ?>"<?php if(isset($this->form_values['company_id']) && $this->form_values['company_id'] == $companyId): ?> selected<?php endif; ?>><?= $companyName ?></option>
                    <?php endforeach; ?>
                </select>
                <?php if (isset($this->errors['company_id'])): ?>
                    <div class="error"><?= $this->errors['company_id'] ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="worksheet_problem_desc">A hiba leírása</label>
                <textarea type="text" id="worksheet_problem_desc" name="worksheet[problem_desc]" class="form-control" rows="5"><?= isset($this->form_values['problem_desc']) ? $this->form_values['problem_desc'] : '' ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label for="worksheet_delivery_date">Kiszállás időpontja</label>
                <div class='input-group date' id='worksheet_delivery_date'>
                    <input type='text' name="worksheet[delivery_date]" id="worksheet_delivery_date" class="form-control datepicker" value="<?= isset($this->form_values['delivery_date']) ? $this->form_values['delivery_date'] : '' ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <?php if (isset($this->errors['delivery_date'])): ?>
                    <div class="error"><?= $this->errors['delivery_date'] ?></div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="worksheet_work_duration">Időtartam</label>
                <input type="text" id="worksheet_work_duration" name="worksheet[work_duration]" class="form-control" value="<?= isset($this->form_values['work_duration']) ? $this->form_values['work_duration'] : '' ?>">
                <?php if (isset($this->errors['work_duration'])): ?>
                    <div class="error"><?= $this->errors['work_duration'] ?></div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Számlázandó?</label>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-default <?php if (isset($this->form_values['billable']) && $this->form_values['billable']): ?>active<?php endif; ?>">
                        <input type="radio" name="worksheet[billable]" id="billable_yes" value="1" autocomplete="off" <?php if (isset($this->form_values['billable']) && $this->form_values['billable']): ?>checked<?php endif; ?>> Igen
                    </label>
                    <label class="btn btn-default <?php if (isset($this->form_values['billable']) && !$this->form_values['billable']): ?>active<?php endif; ?>">
                        <input type="radio" name="worksheet[billable]" id="billable_no" value="0" autocomplete="off" <?php if (isset($this->form_values['billable']) && !$this->form_values['billable']): ?>checked<?php endif; ?>> Nem
                    </label>
                    <?php if (isset($this->errors['billable'])): ?>
                        <div class="error"><?= $this->errors['billable'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="field_wrapper col-md-12">
            <h5 style="font-weight: bold;">Felhasznált anyagok <a href="javascript:void(0);" class="add_button btn-primary btn btn-xs" title="Anyag hozzáadása"><span class="glyphicon glyphicon-plus"></span></a></h5>
            <div class="row hidden">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>anyag neve</label>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>anyag ára</label>
                    </div>
                </div>
            </div>
            <?php if (isset($this->form_values['materials'])): ?>
                <?php foreach ($this->form_values['materials'] as $i => $value): ?>
                <div class="row material_row">
                    <div class="col-xs-12">
                    <h5>
                        <span class="label label-primary">Anyag #<span class="material_number"><? echo $i+1 ?></span></span> <span class="label label-danger"><a href="javascript:void(0);" class="remove_button" style="color: white;"><span class="glyphicon glyphicon-remove"></span></a></span>
                    </h5>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <div class="form-group">
                            <input type="text" name="worksheet[material_name][]" placeholder="neve" class="form-control" value="<?= $value['name'] ?>"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <div class="form-group">
                            <input type="text" name="worksheet[material_price][]" placeholder="ára" class="form-control" value="<?= $value['price'] ?>"/>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="worksheet_work_desc">Elvégzett munka leírása</label>
                <textarea type="text" id="worksheet_work_desc" name="worksheet[work_desc]" class="form-control" rows="5"><?= isset($this->form_values['work_desc']) ? $this->form_values['work_desc'] : '' ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">További intézkedés szükséges</label>
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-default <?php if (isset($this->form_values['needed_action']) && $this->form_values['needed_action']): ?>active<?php endif; ?>">
                        <input type="radio" name="worksheet[needed_action]" id="needed_action_yes" value="1" autocomplete="off" <?php if (isset($this->form_values['needed_action']) && $this->form_values['needed_action']): ?>checked<?php endif; ?>> Igen
                    </label>
                    <label class="btn btn-default <?php if (isset($this->form_values['needed_action']) && !$this->form_values['needed_action']): ?>active<?php endif; ?>">
                        <input type="radio" name="worksheet[needed_action]" id="needed_action_no" value="0" autocomplete="off" <?php if (isset($this->form_values['needed_action']) && !$this->form_values['needed_action']): ?>checked<?php endif; ?>> Nem
                    </label>
                    <?php if (isset($this->errors['needed_action'])): ?>
                        <div class="error"><?= $this->errors['needed_action'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="worksheet_user_name">A munkát végezte</label>
                <input type="hidden" name="worksheet[user_id]" id="worksheet_user_id" class="form-control" value="<?= $this->form_values['user_id'] ?>">
                <input type="text" name="user_name" id="worksheet_user_name" disabled class="form-control" value="<?= $this->user_name ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-md-offset-4">
            <div class="form-group">
                <button class="btn btn-md btn-primary btn-block" type="submit">Mentés</button>
            </div>
        </div>
    </div>
</form>
