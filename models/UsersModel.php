<?php
namespace Models;

class UsersModel extends ModelBase
{
    const TABLE_NAME = 'users';

    public function getUserByEmail($email)
    {
        return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE email = :email', ['email' => $email]);
    }

    public function getUsers()
    {
        return $this->findAll('SELECT * FROM '.self::TABLE_NAME.' ORDER BY name ASC');
    }

    public function getUserById($id)
    {
        return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE id = :id', ['id' => $id]);
    }

    public function updateUser($id, $params)
    {
        $data = array_merge(['id' => $id], $params);
        if (isset($data['password'])) {
            return $this->prepareQuery('UPDATE '.self::TABLE_NAME.' SET name=:name, signature=:signature, password=:password WHERE id=:id', $data);
        } else {
            return $this->prepareQuery('UPDATE '.self::TABLE_NAME.' SET name=:name, signature=:signature WHERE id=:id', $data);
        }
    }

    static function get()
    {
        $class = get_class();
        return new $class;
    }
}