<?php
namespace Models;

class UsedMaterialsModel extends ModelBase
{
    const TABLE_NAME = 'used_materials';

    public function getMaterialsByWorksheetId($id)
    {
        return $this->findAll('SELECT * FROM '.self::TABLE_NAME.' WHERE worksheet_id = :id', ['id' => $id]);
    }

    public function create($params)
    {
        $success = true;
        foreach ($params as $param) {
            $this->prepareQuery('INSERT INTO '.self::TABLE_NAME.' (worksheet_id, name, price) VALUES (:worksheet_id, :name, :price)', $param);
            if (!$this->getLastInsertedId()) {
                $success = false;
            }
        }
        return $success;
    }

    public function update($id, $params)
    {
        $this->delete($id);
        return $this->create($params);
    }

    public function delete($id)
    {
        return $this->prepareQuery('DELETE FROM '.self::TABLE_NAME.' WHERE worksheet_id=:id', ['id' => $id]);
    }

    static function get()
    {
        $class = get_class();
        return new $class;
    }
}