<?php
namespace Models;

abstract class ModelBase
{
    private $pdo;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function findAll($query, $params = [])
    {
        $stmt = $this->prepareQuery($query, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findOne($query, $params = [])
    {
        $stmt = $this->prepareQuery($query, $params);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function count($query, $params = [])
    {
        $stmt = $this->prepareQuery($query, $params);
        return $stmt->rowCount();
    }

    public function getLastInsertedId()
    {
        return $this->pdo->lastInsertId();
    }

    public function prepareQuery($query, $params)
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
        return $stmt;
    }

    private function getConnection()
    {
        try {
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;
            return new \PDO($dsn, DB_USER, DB_PASS);
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }
}
