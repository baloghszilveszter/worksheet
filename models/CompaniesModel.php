<?php
namespace Models;

class CompaniesModel extends ModelBase
{
    const TABLE_NAME = 'companies';

    public function getCompanyById($id)
    {
        return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE id = :id', ['id' => $id]);
    }

    public function getCompanies()
    {
        return $this->findAll('SELECT * FROM '.self::TABLE_NAME.' ORDER BY name ASC');
    }

    public function createCompany($params)
    {
        return $this->prepareQuery('INSERT INTO '.self::TABLE_NAME.' (name, tax_number, address, hourly_wage) VALUES (:name, :tax_number, :address, :hourly_wage)', $params);
    }

    public function updateCompany($id, $params)
    {
        $data = array_merge(['id' => $id], $params);
        return $this->prepareQuery('UPDATE '.self::TABLE_NAME.' SET name=:name, tax_number=:tax_number, address=:address, hourly_wage=:hourly_wage WHERE id=:id', $data);
    }

    public function deleteCompany($id)
    {
        return $this->prepareQuery('DELETE FROM '.self::TABLE_NAME.' WHERE id=:id', ['id' => $id]);
    }

    static function get()
    {
        $class = get_class();
        return new $class;
    }
}