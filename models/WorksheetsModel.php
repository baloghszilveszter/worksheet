<?php
namespace Models;

class WorksheetsModel extends ModelBase
{
    const TABLE_NAME = 'worksheets';

    public function getWorksheetById($id)
    {
        return $this->findOne('SELECT * FROM '.self::TABLE_NAME.' WHERE id = :id', ['id' => $id]);
    }

    public function getYears()
    {
        $query = 'SELECT YEAR(created_at) as created FROM '.self::TABLE_NAME;
        $records = $this->findAll($query.' GROUP BY YEAR(created_at)');
        $years = [];
        foreach ($records as $year) {
            $years[$year['created']] = $year['created'];
        }

        return $years;
    }

    public function getWorksheets($year, $month)
    {
        $query = 'SELECT worksheets.*, companies.hourly_wage, SUM(used_materials.price) as materials_price FROM '.self::TABLE_NAME.
            ' LEFT JOIN '.UsedMaterialsModel::TABLE_NAME.' ON worksheets.id = used_materials.worksheet_id'.
            ' LEFT JOIN '.CompaniesModel::TABLE_NAME.' ON companies.id = worksheets.company_id'.
            ' WHERE YEAR(created_at) = ' . $year;
        if ($month != 'all') {
            $query .= ' AND MONTH(created_at) = ' . $month;
        }
        return $this->findAll($query.' GROUP BY worksheets.id ORDER BY created_at DESC');
    }

    public function createWorksheet($params)
    {
        $materialsNames = [];
        if (isset($params['material_name'])) {
            $materialsNames = $params['material_name'];
            unset($params['material_name']);
        }
        $materialPrices = [];
        if (isset($params['material_price'])) {
            $materialPrices = $params['material_price'];
            unset($params['material_price']);
        }

        $this->prepareQuery('INSERT INTO '.self::TABLE_NAME.' (company_id, user_id, serial_num, problem_desc, delivery_date, work_duration, billable, work_desc, needed_action) VALUES' .
            '(:company_id, :user_id, :serial_num, :problem_desc, :delivery_date, :work_duration, :billable, :work_desc, :needed_action)', $params);

        $insertedId = $this->getLastInsertedId();
        if ($insertedId) {
            $usedMaterials = $this->setMaterialsData($materialsNames, $materialPrices, $insertedId);
            UsedMaterialsModel::get()->create($usedMaterials);
        }
        return $insertedId;
    }

    public function updateWorksheet($id, $params)
    {
        $stmt = false;
        $materialsNames = [];
        if (isset($params['material_name'])) {
            $materialsNames = $params['material_name'];
            unset($params['material_name']);
        }
        $materialPrices = [];
        if (isset($params['material_price'])) {
            $materialPrices = $params['material_price'];
            unset($params['material_price']);
        }

        $data = array_merge(['id' => $id], $params);
        $this->prepareQuery('UPDATE '.self::TABLE_NAME.' SET company_id=:company_id, user_id=:user_id, serial_num=:serial_num, problem_desc=:problem_desc, delivery_date=:delivery_date, work_duration=:work_duration, billable=:billable, work_desc=:work_desc, needed_action=:needed_action WHERE id=:id', $data);

        if ($id) {
            $usedMaterials = $this->setMaterialsData($materialsNames, $materialPrices, $id);
            $stmt = UsedMaterialsModel::get()->update($id, $usedMaterials);
        }
        return $stmt;
    }

    public function setUploaded($id)
    {
        $data = ['id' => $id];
        return $this->prepareQuery('UPDATE '.self::TABLE_NAME.' SET uploaded=1 WHERE id=:id', $data);
    }

    public function deleteWorksheet($id)
    {
        UsedMaterialsModel::get()->delete($id);
        return $this->prepareQuery('DELETE FROM '.self::TABLE_NAME.' WHERE id=:id', ['id' => $id]);
    }

    public function getIncomes()
    {
        $query = 'SELECT companies.name, companies.hourly_wage*worksheets.work_duration + IFNULL((select sum(price) FROM used_materials WHERE worksheet_id = worksheets.id), 0) as sum_price ' .
            'FROM worksheets LEFT JOIN companies ON companies.id = worksheets.company_id WHERE worksheets.uploaded = 1 AND worksheets.billable = 1';
        $result = $this->findAll($query);

        $incomes = [];
        foreach ($result as $row) {
            if (!isset($incomes[$row['name']])) {
                $incomes[$row['name']] = 0;
            }
            $incomes[$row['name']] += $row['sum_price'];
        }
        return $incomes;
    }

    public function getIncomesUploadedStatusCount($uploaded)
    {
        $data = ['uploaded' => (int)$uploaded ];
        $query =  $this->prepareQuery('SELECT COUNT(*) AS "count" from worksheets where uploaded = :uploaded', $data);
        return $query->fetchAll(\PDO::FETCH_ASSOC)[0];
    }

    private function setMaterialsData($materialsNames, $materialPrices, $insertedId)
    {
        $usedMaterials = [];
        foreach ($materialsNames as $index => $name) {
            $price = $materialPrices[$index];
            if ($name != '' && $price != '') {
                $usedMaterials[] = [
                    'worksheet_id' => $insertedId,
                    'name' => $name,
                    'price' => $price,
                ];
            }

        }
        return $usedMaterials;
    }

    static function get()
    {
        $class = get_class();
        return new $class;
    }
}